import bpy
import logging

if __name__ == "__main__":
    logger = logging.getLogger()
else:
    logger = logging.getLogger(__name__)


def get_ptvsd_module():
    try:
        import ptvsd

        return ptvsd
    except ImportError:
        import subprocess

        subprocess.run([bpy.app.binary_path_python, "-m", "pip", "install", "ptvsd"])

    import ptvsd

    return ptvsd


ptvsd = get_ptvsd_module()
PTVSD_PORT = None


class WaitDebugguerOperator(bpy.types.Operator):
    bl_idname = "blender_debug.wait_for_debugguer"
    bl_label = "Wait for python debugguer"
    bl_options = {"REGISTER"}

    def execute(self, context):
        ptvsd.wait_for_attach()

        return {"FINISHED"}


class DebugPanel(bpy.types.Panel):
    """Panel"""

    bl_label = "Debug"
    bl_idname = "DEBUG_PT_settings"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Debug"

    def draw(self, context):
        layout = self.layout
        column = layout.column()
        column.label(text=f"Port {PTVSD_PORT}")
        column.operator(WaitDebugguerOperator.bl_idname, text="Wait for Debugguer")


classes = (
    WaitDebugguerOperator,
    DebugPanel,
)


def setup_python_debugguer(ptvsd_port, wait_for_debugger):
    global PTVSD_PORT
    PTVSD_PORT = ptvsd_port
    ptvsd.enable_attach(address=("localhost", ptvsd_port), redirect_output=True)
    if wait_for_debugger:
        logger.info("Waiting for debugger...")
        ptvsd.wait_for_attach()

    logger.info("Starting ptvsd port %s", ptvsd_port)


def register():
    import os

    setup_python_debugguer(
        int(os.environ.get("C2BA_BLENDER_DEBUG_PORT", 5688)),
        os.environ.get("C2BA_BLENDER_DEBUG_WAIT", "0") == "1",
    )

    for c in classes:
        bpy.utils.register_class(c)


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)


if __name__ == "__main__":
    register()
