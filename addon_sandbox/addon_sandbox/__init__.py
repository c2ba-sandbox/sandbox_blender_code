import logging
import bpy
import bl_ui
from typing import Tuple

bl_info = {
    "name": "Sandbox Addon",
    "description": "My sandbox addon for quick Blender experiments and snippets.",
    "author": "Laurent 'c2ba' NOEL",
    "version": (0, 1, 0),
    "blender": (2, 83, 0),
    "location": "View3D",
    "warning": "This addon is still in development.",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Unknown",
}

__version__ = ".".join(str(i) for i in bl_info["version"])

if __name__ == "__main__":
    logger = logging.getLogger()
else:
    logger = logging.getLogger(__name__)


class HelloWorldOperator(bpy.types.Operator):
    bl_idname = "c2ba_sandbox.hello_world"
    bl_label = "Hello World"
    bl_description = "Basic Hello World Operator"
    bl_options = {"REGISTER"}

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context: bpy.types.Context):
        self.report({"INFO"}, "Hello World !")
        return {"FINISHED"}

    @classmethod
    def description(cls, context, properties):
        return "This is the Hello World description"


test_space_3d_draw_handlers = dict()


class TestSpace3DDrawHandlerOperator(bpy.types.Operator):
    bl_idname = "c2ba_sandbox.test_space_3d_draw_handler"
    bl_label = "Test Space 3D Draw Handler"
    bl_description = "Connect draw handlers to check if they are called"
    bl_options = {"REGISTER"}

    @classmethod
    def draw_callback(
        cls,
        window: bpy.types.Window,
        space: bpy.types.SpaceView3D,
        origin: Tuple[float, float],
    ):
        import bgl
        import gpu
        from gpu_extras.batch import batch_for_shader

        if space not in (area.spaces.active for area in window.screen.areas):
            space.draw_handler_remove(test_space_3d_draw_handlers[space], "WINDOW")
            del test_space_3d_draw_handlers[space]
            return

        shader = gpu.shader.from_builtin("3D_UNIFORM_COLOR")
        shader.bind()
        shader.uniform_float("color", (1, 1, 1, 1))

        bgl.glLineWidth(3)
        bgl.glEnable(bgl.GL_DEPTH_TEST)
        bgl.glEnable(bgl.GL_BLEND)
        bgl.glEnable(bgl.GL_LINE_SMOOTH)

        indices = ((0, 1),)
        position = [(origin[0], origin[1], 0), (origin[0], origin[1], 10)]
        batch = batch_for_shader(shader, "LINES", {"pos": position}, indices=indices)
        batch.draw(shader)

        print(window, space)

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context: bpy.types.Context):
        offset = 0.0
        for wm in bpy.data.window_managers:
            for window in wm.windows:
                for area in window.screen.areas:
                    if area.type == "VIEW_3D":
                        if area.spaces.active not in test_space_3d_draw_handlers:
                            test_space_3d_draw_handlers[
                                area.spaces.active
                            ] = area.spaces.active.draw_handler_add(
                                TestSpace3DDrawHandlerOperator.draw_callback,
                                (window, area.spaces.active, (offset, offset)),
                                "WINDOW",
                                "POST_VIEW",
                            )
                            offset += 1.0
        return {"FINISHED"}

    @classmethod
    def description(cls, context, properties):
        return "This is the Hello World description"


class SandboxSubMenu(bpy.types.Menu):
    bl_idname = "c2ba_sandbox.menu.submenu"
    bl_label = "Submenu"

    def draw(self, context: bpy.types.Context):
        layout = self.layout
        layout.operator(HelloWorldOperator.bl_idname)


class SandboxMenu(bpy.types.Menu):
    bl_idname = "c2ba_sandbox.menu"
    bl_label = "c2ba Sandbox"

    def draw(self, context: bpy.types.Context):
        layout = self.layout
        layout.operator(HelloWorldOperator.bl_idname)
        layout.operator(TestSpace3DDrawHandlerOperator.bl_idname)
        layout.separator()
        layout.menu(SandboxSubMenu.bl_idname)


def draw_sandbox_menu(
    self: bl_ui.space_topbar.TOPBAR_MT_editor_menus, context: bpy.types.Context
):
    """
    Draw the menu
    """
    layout = self.layout
    layout.menu(SandboxMenu.bl_idname)


classes = (
    HelloWorldOperator,
    SandboxSubMenu,
    SandboxMenu,
    TestSpace3DDrawHandlerOperator,
)
register_factory, unregister_factory = bpy.utils.register_classes_factory(classes)


def register():
    logging.info(f"Register {__name__} version {__version__}")
    register_factory()
    bpy.types.TOPBAR_MT_editor_menus.prepend(draw_sandbox_menu)


def unregister():
    logging.info(f"Unregister {__name__} version {__version__}")
    bpy.types.TOPBAR_MT_editor_menus.remove(draw_sandbox_menu)
    unregister_factory()


if __name__ == "__main__":
    register()
