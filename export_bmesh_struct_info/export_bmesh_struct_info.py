import logging
import sys
import json
import pprint
import builtins
from pathlib import Path

log = logging.getLogger(__file__)

if '--debug' in sys.argv:
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)


def usage():
    print(
        f"\nUsage: \n\tblender --background -noaudio --python {__file__} [-- [options]]")
    print("Options:")
    print("\t--debug:           sets the logging level to DEBUG (lots of additional info)")


builtin_types = [getattr(builtins, d).__name__ for d in dir(
    builtins) if isinstance(getattr(builtins, d), type)]

method_types = ['method-wrapper', 'builtin_function_or_method']
exclude_types = builtin_types + method_types + ["Vector", "type"]


def recursive_get_struct(d, object, max_rec=-1):
    t = type(object)
    type_name = t.__name__

    if type_name in d:
        return

    d[type_name] = {
        "name": type_name
    }

    if max_rec > 1 or max_rec < 0:
        if not type_name in exclude_types:
            d[type_name]["methods"] = []
            d[type_name]["attributes"] = {}
            for key, member in t.__dict__.items():
                try:
                    member_ref = getattr(object, key)
                    t = type(member_ref)
                    if t.__name__ in method_types:
                        d[type_name]["methods"].append(key)
                    else:
                        d[type_name]["attributes"][key] = t.__name__
                        if not t.__name__ in exclude_types:
                            recursive_get_struct(
                                d, member_ref, max_rec - 1)
                except Exception as e:
                    print(e)
                    pass
            try:
                if "__getitem__" in d[type_name]["methods"]:
                    if "ensure_lookup_table" in d[type_name]["methods"]:
                        object.ensure_lookup_table()
                    recursive_get_struct(d, object[0], max_rec - 1)
            except Exception as e:
                print(type_name)
                print(object)
                print(e)
                pass


def main():
    try:
        import bpy
        import bmesh

        bpy.ops.mesh.primitive_uv_sphere_add()
        # bpy.ops.mesh.primitive_cube_add()

        mesh = bpy.data.meshes[0]
        bm = bmesh.new()
        bm.from_mesh(mesh)

        bm.verts.ensure_lookup_table()
        bm.edges.ensure_lookup_table()
        bm.faces.ensure_lookup_table()

        d = {}
        recursive_get_struct(d, bm)

        for layer in d['BMLayerAccessVert']['attributes']:
            if layer != '__doc__':
                layer_collection = getattr(bm.verts.layers, layer)
                try:
                    layer_collection.new()
                    obj = bm.verts[0][layer_collection[0]]
                    t = type(obj)
                    if not t.__name__ in exclude_types:
                        recursive_get_struct(d, obj)
                    d['BMLayerAccessVert']['attributes'][layer] += "<" + \
                        t.__name__ + ">"
                except Exception as e:
                    print(f"{layer} {e}")
                    # error "paint_mask layers.new(): is a singleton, use verify() instead":
                    layer_collection.verify()
                    obj = bm.verts[0][layer_collection[0]]
                    t = type(obj)
                    if not t.__name__ in exclude_types:
                        recursive_get_struct(d, obj)
                    d['BMLayerAccessVert']['attributes'][layer] += "<" + \
                        t.__name__ + ">"

        for layer in d['BMLayerAccessEdge']['attributes']:
            if layer != '__doc__':
                layer_collection = getattr(bm.edges.layers, layer)
                try:
                    layer_collection.new()
                    obj = bm.edges[0][layer_collection[0]]
                    t = type(obj)
                    if not t.__name__ in exclude_types:
                        recursive_get_struct(d, obj)
                    d['BMLayerAccessEdge']['attributes'][layer] += "<" + \
                        t.__name__ + ">"
                except Exception as e:
                    print(f"{layer} {e}")

        for layer in d['BMLayerAccessFace']['attributes']:
            if layer != '__doc__':
                layer_collection = getattr(bm.faces.layers, layer)
                try:
                    layer_collection.new()
                    obj = bm.faces[0][layer_collection[0]]
                    t = type(obj)
                    if not t.__name__ in exclude_types:
                        recursive_get_struct(d, obj)
                    d['BMLayerAccessFace']['attributes'][layer] += "<" + \
                        t.__name__ + ">"
                except Exception as e:
                    print(f"{layer} {e}")

        for layer in d['BMLayerAccessLoop']['attributes']:
            if layer != '__doc__':
                layer_collection = getattr(bm.loops.layers, layer)
                try:
                    layer_collection.new()
                    obj = bm.faces[0].loops[0][layer_collection[0]]
                    t = type(obj)
                    if not t.__name__ in exclude_types:
                        recursive_get_struct(d, obj)
                    d['BMLayerAccessLoop']['attributes'][layer] += "<" + \
                        t.__name__ + ">"
                except Exception as e:
                    print(f"{layer} {e}")

        bm.free()

        with open(Path(__file__).parent / "export.json", "w") as f:
            json.dump(d, f, indent=2)

        # s = json.dumps(dict(type(bm).__dict__), indent=2)

        # bm_class = dict(type(bm).__dict__)
        # for key in bm_class:
        #     member = bm_class[key]
        #     descr = str(member)

        #     pprint.pprint(key)
        #     pprint.pprint(type(getattr(bm, key)))
        #     pprint.pprint(type(getattr(bm, key)).__name__ ==
        #                   "builtin_function_or_method")

        #     if key == "__doc__":
        #         print("LOL")
        #         # pprint.pprint(dict(type(member).__dict__))
        #         print("mdr")
        #     pprint.pprint("")

    except ImportError:
        log.warning("  skipping, not running in Blender")
        usage()
        sys.exit(2)


if __name__ == '__main__':
    main()
