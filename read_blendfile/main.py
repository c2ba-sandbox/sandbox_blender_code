import struct
import sys
import pprint
import json

# This experiment is done by following the document mystery_of_the_blend.html
# that can be found here https://developer.blender.org/diffusion/B/browse/master/doc/blender_file_format/

FILE_PATH = sys.argv[1]  # Should be a .blend file

with open(FILE_PATH, 'rb') as f:
    buffer = f.read()

print(len(buffer))
print(type(buffer))

offset = 0

# Reading File-header

# Quoted from document:
#
# The first 12 bytes of every blend-file is the file-header. The file-header has information on Blender (version-number)
# and the PC the blend-file was saved on (pointer-size and endianness). This is required as all data inside the
# blend-file is ordered in that way, because no translation or transformation is done during saving. The next table
# describes the information in the file-header.

# should display (b'B', b'L', b'E', b'N', b'D', b'E', b'R')
result = struct.unpack_from('7c', buffer, offset)
offset += 7
print(result)

# should display '_' for 32 bit, '-' for 64 bit pointers
result = struct.unpack_from('1c', buffer, offset)
offset += 1
print(result)

pointer_size = 4 if result[0] == '_' else 8
print(pointer_size)

# should display 'v' for little endian, 'V' for big endian
result = struct.unpack_from('1c', buffer, offset)
offset += 1
print(result)

# should display version number of Blender the file was created in
result = struct.unpack_from('3c', buffer, offset)
offset += 3
print(result)

assert(offset == 12)

# Reading File-blocks
# File-blocks contain a "file-block header" and "file-block data".
# The file ends with a block having code 'ENDB'

# Quoted from document:
#
# The file-block-header describes:
# - the type of information stored in the file-block
# - the total length of the data
# - the old memory pointer at the moment the data was written to disk
# - the number of items of this information
# As we can see below, depending on the pointer-size stored in the file-header, a file-block-header
# can be 20 or 24 bytes long, hence it is always aligned at 4 bytes.


class FileBlockHeader:
    code: str  # File-block identifier
    size: int  # Total length of the data after the file-block-header
    old_memory_address: int  # Memory address the structure was located when written to disk
    sdna_index: int  # Index of the SDNA structure
    count: int  # Number of structure located in this file-block
    offset: int  # Offset in bytes array

    def __init__(self, code: str, size: int, old_memory_address: int, sdna_index: int, count: int, offset: int):
        self.code = code
        self.size = size
        self.old_memory_address = old_memory_address
        self.sdna_index = sdna_index
        self.count = count
        self.offset = offset


file_blocks = []
dna1_file_block_header = None

while True:
    result = struct.unpack_from('4c', buffer, offset)
    offset += 4

    code = b''.join(result).decode()

    result = struct.unpack_from('1i', buffer, offset)
    offset += 4

    size = result[0]

    result = struct.unpack_from(
        '1i' if pointer_size == 4 else '1q', buffer, offset)
    offset += pointer_size

    old_memory_address = result[0]

    result = struct.unpack_from('1i', buffer, offset)
    offset += 4

    sdna_index = result[0]

    result = struct.unpack_from('1i', buffer, offset)
    offset += 4

    count = result[0]

    file_blocks.append(FileBlockHeader(
        code, size, old_memory_address, sdna_index, count, offset))

    offset += size

    if code == 'ENDB':
        break

    if code == 'DNA1':
        dna1_file_block_header = file_blocks[-1]

assert(dna1_file_block_header != None)

print(dna1_file_block_header.__dict__)

# Read DNA1 file-block data

offset = dna1_file_block_header.offset
result = struct.unpack_from('4c', buffer, offset)
offset += 4

identifier = b''.join(result).decode()

result = struct.unpack_from('4c', buffer, offset)
offset += 4

name_identifier = b''.join(result).decode()

result = struct.unpack_from('1i', buffer, offset)
offset += 4

names_count = result[0]

print(
    f"identifier = {identifier} name_identifier = {name_identifier} names_count = {names_count}")

names = []


def read_string(buffer: bytes, offset: int):
    result = ""
    c = struct.unpack_from('c', buffer, offset)[0].decode()
    while c != "\0":
        result += c
        offset += 1
        c = struct.unpack_from('c', buffer, offset)[0].decode()
    offset += 1
    return result, offset


def align_offset(offset):
    trim = offset % 4
    if trim != 0:
        offset += 4 - trim
    return offset


for i in range(names_count):
    name, offset = read_string(buffer, offset)
    names.append(name)

offset = align_offset(offset)

result = struct.unpack_from('4c', buffer, offset)
offset += 4
type_identifier = b''.join(result).decode()

result = struct.unpack_from('1i', buffer, offset)
offset += 4

types_count = result[0]

print(f"type_identifier = {type_identifier} types_count = {types_count}")

types = []

for i in range(types_count):
    type, offset = read_string(buffer, offset)
    types.append(type)

offset = align_offset(offset)

result = struct.unpack_from('4c', buffer, offset)
offset += 4

length_identifier = b''.join(result).decode()

print(f"length_identifier = {length_identifier}")

type_lengths = []

for i in range(types_count):
    result = struct.unpack_from('1h', buffer, offset)
    offset += 2

offset = align_offset(offset)

result = struct.unpack_from('4c', buffer, offset)
offset += 4

structure_identifier = b''.join(result).decode()

result = struct.unpack_from('1i', buffer, offset)
offset += 4

structure_count = result[0]

print(
    f"structure_identifier = {structure_identifier} structure_count = {structure_count}")


class StructField:
    type_index: int
    name_index: int

    def __init__(self, type_index, name_index):
        self.type_index = type_index
        self.name_index = name_index


class Struct:
    type_index: int
    field_count: int
    fields: list

    def __init__(self, type_index, name_index, fields):
        self.type_index = type_index
        self.name_index = name_index
        self.fields = fields


structures = []

for i in range(structure_count):
    type_index = struct.unpack_from('1h', buffer, offset)[0]
    offset += 2

    field_count = struct.unpack_from('1h', buffer, offset)[0]
    offset += 2

    fields = []

    for j in range(field_count):
        field_type_index = struct.unpack_from('1h', buffer, offset)[0]
        offset += 2

        field_name_index = struct.unpack_from('1h', buffer, offset)[0]
        offset += 2

        fields.append(StructField(field_type_index, field_name_index))

    structures.append(Struct(type_index, field_count, fields))

# Now that we have everything, lets store the whole structure in a json
